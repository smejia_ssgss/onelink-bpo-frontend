jQuery.extend(jQuery.fn.dataTableExt.oSort, {
    
    "date-custom-pre": function (a) {
        if (a == null || a == "") {
            return 0;
        }
        var datea = a.split('/');
        return new Date(datea[2], datea[1], datea[0]);
    },

    "date-custom-asc": function (a, b) {
        return ((a < b) ? -1 : ((a > b) ? 1 : 0));
    },

    "date-custom-desc": function (a, b) {
        return ((a < b) ? 1 : ((a > b) ? -1 : 0));
    }
});