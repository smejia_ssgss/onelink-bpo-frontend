export class TipoDocumentoModel {
  public id: number = 0;
  public codigo: string = null;
  public nombre: string = null;
}
