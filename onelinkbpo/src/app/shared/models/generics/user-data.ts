export class InfoUsuario {
    public logged: boolean;
    public message: string;
    public token: string;
    public id: number;
    public userName: string;
    public role: number;
    public personaId: number;
}