export class Result {
    public successful: boolean = false;
    public message: string = "";
}