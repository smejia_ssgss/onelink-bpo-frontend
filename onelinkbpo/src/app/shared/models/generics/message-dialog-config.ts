export enum MessageType {
    Info,
    Warning,
    Error,
    Success,
    Confirm
}

export interface MessageDialogData {
    headerText?: string;
    content: string;
    type: MessageType;
    customActionLabels?: { cancel?: string, accept?: string };
}
