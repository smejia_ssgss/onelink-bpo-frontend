import { AreaModel } from "../area/area.model";

export class SubareaModel {
  public id: number = 0;
  public nombre: string = null;
  public areaId: number;
  public areas: AreaModel;
}
