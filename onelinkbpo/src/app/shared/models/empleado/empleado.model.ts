import { SubareaModel } from "../subarea/subarea.model";
import { TipoDocumentoModel } from "../tipo-documento/tipo-documento.model";

export class EmpleadoModel {
  public id: number = 0;
  public numeroDocumento: string;
  public tipoDocumentoId: number;
  public nombres: string;
  public apellidos: string;
  public areaId: number;
  public subareaId: number;
  public subareas: SubareaModel;
  public tipoDocumentos: TipoDocumentoModel;
}
