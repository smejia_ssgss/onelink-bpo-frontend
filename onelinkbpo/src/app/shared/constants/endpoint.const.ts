export const ApiEndpoint = {
  default: '/empleado',
  area: '/area',
  subarea: '/subarea',
  tipoDocumento: '/tipoDocumento'
}
