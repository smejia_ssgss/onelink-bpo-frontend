import { Directive, ElementRef, Input, OnInit, OnDestroy } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';

@Directive({
  selector: '[appCustomDatatable]'
})
export class CustomDatatableDirective extends DataTableDirective implements OnInit, OnDestroy {
  @Input()
  public customOptions: DataTables.Settings;

  public trigger: Subject<void> = new Subject();

  private defaultOptions: any;

  constructor(private element: ElementRef<any>) {
    super(element);
    this.dtTrigger = this.trigger;

    this.defaultOptions = {
      info: false,
      search: {
        caseInsensitive: true
      },
      pagingType: 'simple_numbers',
      language: {
        paginate: {
          first: 'Primera Página',
          previous: 'Anterior',
          next: 'Siguiente',
          last: 'Última Página'
        },
        aria: {
          paginate: {
            first: 'Primera Página',
            previous: 'última Página',
            next: 'Siguiente',
            last: 'Anterior'
          },
          sortAscending: ": Ordenar ascendente",
          sortDescending: ": Ordenar descendente"
        },
        emptyTable: "No hay registros disponibles",
        zeroRecords: "No hay registros a mostrar",
        search: "Buscar",
        info: "Mostrando _START_ a _END_ de _TOTAL_ registros",
        infoEmpty: "No hay entradas",
        infoFiltered: "(Filtrado d _MAX_ registros totales)",
        lengthMenu: "Registros a mostrar _MENU_",
        loadingRecords: "Cargando...",
        processing: "Procesando..."
      },
      dom: 'Bfrtip',
      buttons: [
        {
          extend: 'excelHtml5',
          className: 'btn btn-dark',
          exportOptions: {
            columns: 'th:not(:last-child)'
          }
        },
        {
          extend: 'csvHtml5',
          className: 'btn btn-dark',
          exportOptions: {
            columns: 'th:not(:last-child)'
          }
        },
        {
          extend: 'pdfHtml5',
          className: 'btn btn-dark',
          exportOptions: {
            columns: 'th:not(:last-child)'
          }
        }
      ],
      order: [[ 0, 'desc' ]],
      pageLength: 10
    };
  }

  ngOnInit(): void {
    this.dtOptions = Object.assign(this.defaultOptions, this.customOptions);
    super.ngOnInit();
  }

  ngOnDestroy(): void {
    this.trigger.unsubscribe();
    super.ngOnDestroy();
  }

  public renderTable(destroyCurrent: boolean = true) {
    if (this.dtInstance) {
      this.dtInstance.then((instance: DataTables.Api) => {
        if (destroyCurrent) {
          instance.destroy();
        }
        this.trigger.next();
      });
    } else {
      this.trigger.next();
    }
  }
}
