import { animate, style, transition, trigger } from '@angular/animations';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subject, Subscription } from 'rxjs';

import { LoaderService } from '../../services/subjects/loader.service';

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.scss'],
  animations: [
    trigger(
      'inOutAnimation', [
      transition(':enter', [
        style({ opacity: 0 }),
        animate('500ms', style({ opacity: 1 }))
      ]),
      transition(':leave', [
        style({ opacity: 1 }),
        animate('100ms', style({ opacity: 0 }))
      ])
    ])
  ]
})
export class LoaderComponent implements OnInit, OnDestroy {
  statusSubs: Subscription;
  isLoading: boolean;
  private quantityRequest;

  constructor(private status: LoaderService) {
    this.quantityRequest = 0;
    this.isLoading = false;
  }

  private showLoader(value) {
    // const loader = document.getElementById("audisoft-request-loader");
    if (!this.quantityRequest || this.quantityRequest < 0) {
      this.quantityRequest = 0;
    }

    if (value) {
      this.quantityRequest++;
      this.isLoading = true;
      // loader.classList.remove("hide-loading");
      // loader.classList.add("show-loading");
    } else {
      this.quantityRequest--;
      if (this.quantityRequest <= 0) {
        this.isLoading = false;
        // loader.classList.remove("show-loading");
        // loader.classList.add("hide-loading");
      }
    }
  }

  ngOnInit() {
    this.statusSubs = this.status.statusLoader().subscribe(status => {
      if (!(this.status.keepHiddenLoadingRequest && status)) {
        this.showLoader(status);
      }
    });
  }

  ngOnDestroy(): void {
    this.statusSubs.unsubscribe();
  }
}
