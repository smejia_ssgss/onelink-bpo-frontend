import { Component, ElementRef, Inject, OnInit, Renderer2 } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { MessageDialogData, MessageType } from '../../models/generics/message-dialog-config';

@Component({
  selector: 'app-message-dialog',
  templateUrl: './message-dialog.component.html',
  styleUrls: ['./message-dialog.component.scss']
})
export class MessageDialogComponent implements OnInit {
  public classType: string;
  public types = MessageType;
  public type: MessageType;

  constructor(private element: ElementRef, private renderer: Renderer2, @Inject(MAT_DIALOG_DATA) public data: MessageDialogData,
    public dialogRef: MatDialogRef<MessageDialogComponent>) {
      this.setDialogType(this.data.type);
  }

  ngOnInit(): void {
  }

  private setDialogType(type: MessageType) {
    this.type = type;
    switch (type) {
      case MessageType.Info:
      case MessageType.Confirm:
        this.renderer.addClass(this.element.nativeElement, "modal-primary");
        this.classType = `btn btn-primary`;
        break;
      case MessageType.Warning:
        this.renderer.addClass(this.element.nativeElement, "modal-warning");
        this.classType = `btn btn-warning`;
        break;
      case MessageType.Success:
        this.renderer.addClass(this.element.nativeElement, "modal-success");
        this.classType = `btn btn-success`;
        break;
      case MessageType.Error:
        this.renderer.addClass(this.element.nativeElement, "modal-danger");
        this.classType = `btn btn-danger`;
        break;
      default:
        this.renderer.addClass(this.element.nativeElement, "modal-primary");
        this.classType = `btn btn-primary`;
        break;
    }
  }
}
