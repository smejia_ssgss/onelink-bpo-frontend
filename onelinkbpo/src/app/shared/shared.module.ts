import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { MatDialogModule } from '@angular/material/dialog';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { DataTablesModule } from 'angular-datatables';
import { CustomDatatableDirective } from './directives/custom-datatable.directive';
import { MessageDialogComponent } from './components/message-dialog/message-dialog.component';
import { LoaderComponent } from './components/loader/loader.component';
import { MapArrayPipe } from './pipes/map-array.pipe';
import { TimeSpanToDatePipe } from './pipes/time-span-to-date.pipe';
import { SearcherPipe } from './pipes/searcher.pipe';

@NgModule({
  declarations: [
    CustomDatatableDirective,
    MessageDialogComponent,
    LoaderComponent,
    MapArrayPipe,
    TimeSpanToDatePipe,
    SearcherPipe
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    DataTablesModule,
    MatDialogModule,
    MatProgressSpinnerModule
  ],
  exports: [
    CustomDatatableDirective,
    LoaderComponent,
    MapArrayPipe,
    TimeSpanToDatePipe,
    SearcherPipe,
  ]
})
export class SharedModule { }
