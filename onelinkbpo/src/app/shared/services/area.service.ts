import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApiEndpoint } from '../constants/endpoint.const';
import { AreaModel } from '../models/area/area.model';
import { BaseService } from './base.service';

@Injectable({
  providedIn: 'root'
})
export class AreaService extends BaseService<AreaModel> {
  constructor(protected http: HttpClient) {
    super(http, ApiEndpoint.area);
  }
}
