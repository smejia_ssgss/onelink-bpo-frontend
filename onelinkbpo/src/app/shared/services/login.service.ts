import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

import { environment } from 'src/environments/environment';
import { InfoUsuario } from '../models/generics/user-data';
import { StoragesIds } from '../enums/storageIds.enm';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  public get currentUser(): InfoUsuario {
    return this._currentUser;
  }
  
  public set currentUser(value: InfoUsuario) {
    this._currentUser = value;
    sessionStorage.setItem(StoragesIds.ssInfoUser, JSON.stringify(value));
  }
  
  private _currentUser: InfoUsuario;
  private apiClientURL: string;

  constructor(protected _http: HttpClient) {
    this.apiClientURL = environment.API_URL;
    const value = sessionStorage.getItem(StoragesIds.ssInfoUser);
    if (value) {
      this._currentUser = JSON.parse(value);
    }
  }

  public login(user: string, password: string):Observable<InfoUsuario> {
    return this._http.post<InfoUsuario>(`${this.apiClientURL}/authentication/login`, {
      Us: user,
      Psw: password
    }).pipe(map(data => {
      sessionStorage.setItem(StoragesIds.ssToken, data.token);
      data.token = "";
      this.currentUser = data;

      return data;
    }));
  }

  public logout(): Observable<void> {
    return this._http.post<void>(`${this.apiClientURL}/authentication/logout`, null).pipe(map(() => {
      sessionStorage.removeItem(StoragesIds.ssToken);
    }));
  }

  requestRecoveryPassword(email: string, user: string): Observable<void> {
    return this._http.post<void>(`${this.apiClientURL}/authentication/recover-password`, {
      user, 
      email
    });
  }

  resetPassword(user: string, newPassword: string, code: string): Observable<boolean> {
    return this._http.put<boolean>(`${this.apiClientURL}/authentication/reset-password`, {
      user,
      newPassword,
      code
    });
  }
}
