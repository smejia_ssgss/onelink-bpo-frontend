import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { SubareaModel } from '../models/subarea/subarea.model';
import { BaseService } from './base.service';
import { ApiEndpoint } from '../constants/endpoint.const';

@Injectable({
  providedIn: 'root'
})
export class SubareaService extends BaseService<SubareaModel>{
  private apiClientURL: string;

  constructor(protected http: HttpClient) {
    super(http, ApiEndpoint.subarea)
    this.apiClientURL = environment.API_URL;
  }

  getAllByAreaId(id: number): Observable<Array<SubareaModel>> {
    return this.http.get<Array<SubareaModel>>(`${this.apiClientURL}${ApiEndpoint.subarea}/getAllByAreaId/${id}`);
  }
}
