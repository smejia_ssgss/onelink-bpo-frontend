import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoaderService {
  public keepHiddenLoadingRequest: boolean;
  private requestInFlight: BehaviorSubject<boolean>;

  constructor() {
    this.keepHiddenLoadingRequest = false;
    this.requestInFlight = new BehaviorSubject(false);
  }

  /**
   * Emits event to determine if loader should be shown.
   * USED ONLY FOR HTTP REQUESTS, it is called automatically by the request
   * @param inFlight Indicates if the load should be shown
   */
  showHideLoader(inFlight: boolean) {
    this.requestInFlight.next(inFlight);
  }

  /**
   * USED ONLY FOR HTTP REQUESTS
   */
  statusLoader(): Observable<boolean> {
    return this.requestInFlight.asObservable();
  }
}
