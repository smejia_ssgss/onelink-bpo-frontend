import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApiEndpoint } from '../constants/endpoint.const';
import { TipoDocumentoModel } from '../models/tipo-documento/tipo-documento.model';
import { BaseService } from './base.service';

@Injectable({
  providedIn: 'root'
})
export class TipoDocumentoService extends BaseService<TipoDocumentoModel>{
  constructor(protected http: HttpClient) {
    super(http, ApiEndpoint.tipoDocumento)
  }
}
