import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { environment } from 'src/environments/environment';
import { ApiEndpoint } from '../constants/endpoint.const';
import { Result } from '../models/generics/result';

@Injectable({
  providedIn: 'root'
})
export abstract class BaseService<TModel> {
  private apiRoot: string;

  constructor(protected http: HttpClient, @Inject('actionClienteURL') private actionClienteURL: string = ApiEndpoint.default) {
    this.apiRoot = environment.API_URL;
  }

  getAll(): Observable<Array<TModel>> {
    return this.http.get<Array<TModel>>(`${this.apiRoot}${this.actionClienteURL}`);
  }

  getById(id: number): Observable<TModel> {
    return this.http.get<TModel>(`${this.apiRoot}${this.actionClienteURL}/${id}`);
  }

  save(documentType: TModel): Observable<Result> {
    return this.http.post<Result>(`${this.apiRoot}${this.actionClienteURL}/save`, documentType);
  }

  update(documentType: TModel): Observable<Result> {
    return this.http.put<Result>(`${this.apiRoot}${this.actionClienteURL}/update`, documentType);
  }

  delete(id: number): Observable<Result> {
    return this.http.delete<Result>(`${this.apiRoot}${this.actionClienteURL}/delete/${id}`);
  }
}
