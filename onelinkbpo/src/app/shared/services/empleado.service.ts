import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApiEndpoint } from '../constants/endpoint.const';
import { EmpleadoModel } from '../models/empleado/empleado.model';
import { BaseService } from './base.service';

@Injectable({
  providedIn: 'root'
})
export class EmpleadoService extends BaseService<EmpleadoModel> {
  constructor(protected http: HttpClient) {
    super(http, ApiEndpoint.default)
  }
}
