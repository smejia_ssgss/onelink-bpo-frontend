import { Pipe, PipeTransform } from '@angular/core';
import { TimeSpan } from '../models/generics/time-span';

@Pipe({
  name: 'timeSpanToDate'
})
export class TimeSpanToDatePipe implements PipeTransform {

  transform(value: TimeSpan | string): Date {
    if (typeof value === 'string') {
      value = TimeSpan.convertFromString(value);
    }

    const date = new Date();
    date.setHours(value.hours);
    date.setMinutes(value.minutes);
    date.setSeconds(value.seconds);
    date.setMilliseconds(value.milliseconds);

    return date;
  }

}
