import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'mapArray'
})
export class MapArrayPipe implements PipeTransform {

  transform(value: Array<any>, keys: Array<string> | string, separator: string = ' '): Array<any> {
    if (value) {
      if (keys && typeof keys === "string") {
        return value.map(item => item[keys]);
      } else if (keys && Array.isArray(keys)) {
        return value.map(item => keys.map(k => item[k]).join(separator));
      } else {
        return value;
      }
    }

    return [];
  }
}
