import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'searcher'
})
export class SearcherPipe implements PipeTransform {

  transform(values: Array<any>, searchText: string, fields?: Array<string> | string): Array<any> {
    if (values) {
      if (searchText) {
        const filterValue = searchText.toLowerCase();
        if (fields && typeof fields === "string") {
          return values.filter(item => item[fields].toLowerCase().includes(filterValue));
        } else if (fields && Array.isArray(fields)) {
          return values.filter(item => fields.some(f => item[f].toLowerCase().includes(filterValue)));
        } else {
          return values.filter(item => item.toLowerCase().includes(filterValue));
        }
      } else {
        return values;
      }
    }

    return [];
  }

}
