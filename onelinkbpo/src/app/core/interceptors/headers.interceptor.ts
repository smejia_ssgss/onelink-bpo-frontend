import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpHeaders
} from '@angular/common/http';
import { Observable } from 'rxjs';

import { StoragesIds } from 'src/app/shared/enums/storageIds.enm';

@Injectable()
export class HeadersInterceptor implements HttpInterceptor {

  constructor() { }

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    let token: string = sessionStorage.getItem(StoragesIds.ssToken);

    let headers: HttpHeaders = new HttpHeaders();
    headers = request.headers;

    if (token != null && token != undefined && token.trim() != '') {
      headers = headers.set('Authorization', 'Bearer ' + token);
    }

    if (localStorage.getItem(StoragesIds.lsYearFilter)) {
      headers = headers.set('year', localStorage.getItem(StoragesIds.lsYearFilter));
    }

    request = request.clone({ withCredentials: true, headers: headers });

    return next.handle(request);
  }
}
