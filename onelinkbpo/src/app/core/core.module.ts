import { NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
  AppAsideModule,
  AppHeaderModule,
  AppSidebarModule,
} from '@coreui/angular';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';

import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};

import { FullLayoutComponent } from './components/layout/full-layout/full-layout.component';
import { SimpleLayoutComponent } from './components/layout/simple-layout/simple-layout.component';
import { HeadersInterceptor } from './interceptors/headers.interceptor';
import { LoginComponent } from './components/login/login.component';
import { HeaderLayoutComponent } from './components/layout/header-layout/header-layout.component';
import { FooterLayoutComponent } from './components/layout/footer-layout/footer-layout.component';
import { SidebarNavLayoutComponent } from './components/layout/sidebar-nav-layout/sidebar-nav-layout.component';
import { BreadcrumbLayoutComponent } from './components/layout/breadcrumb-layout/breadcrumb-layout.component';
import { LoaderInterceptor } from './interceptors/loader.interceptor';
import { ErrorInterceptor } from './interceptors/error.interceptor';
import { AsideLayoutComponent } from './components/layout/aside-layout/aside-layout.component';

@NgModule({
  declarations: [
    FullLayoutComponent,
    SimpleLayoutComponent,
    LoginComponent,
    HeaderLayoutComponent,
    FooterLayoutComponent,
    SidebarNavLayoutComponent,
    BreadcrumbLayoutComponent,
    AsideLayoutComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    ReactiveFormsModule,
    MatInputModule,
    MatFormFieldModule,
    AppHeaderModule,
    AppSidebarModule,
    AppAsideModule,
    BsDropdownModule.forRoot(),
    PerfectScrollbarModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: LoaderInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: HeadersInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
  ]
})
export class CoreModule {
  constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
    if (parentModule) {
      throw new Error(
        'CoreModule is already loaded. Import it in the AppModule only'
      );
    }
  }
}
