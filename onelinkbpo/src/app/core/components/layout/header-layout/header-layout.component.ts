import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from 'src/app/shared/services/login.service';

@Component({
  selector: 'app-header-layout',
  templateUrl: './header-layout.component.html',
  styleUrls: ['./header-layout.component.scss']
})
export class HeaderLayoutComponent implements OnInit, OnDestroy {
  public showAsideToggler: boolean;
  public get userName() : string {
    return this.login.currentUser.userName;
  }

  constructor(private router: Router, private login: LoginService) {
    this.showAsideToggler = true;
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
  }

  logout() {
    this.login.logout().subscribe(() => {
      this.router.navigate(['login']);
    });
  }
}
