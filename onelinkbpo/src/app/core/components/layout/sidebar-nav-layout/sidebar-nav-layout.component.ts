import { Component, OnInit } from '@angular/core';
import { LoginService } from 'src/app/shared/services/login.service';
import { navItems } from './_nav';

@Component({
  selector: 'app-sidebar-nav-layout',
  templateUrl: './sidebar-nav-layout.component.html',
  styleUrls: ['./sidebar-nav-layout.component.scss']
})
export class SidebarNavLayoutComponent implements OnInit {
  public sidebarMinimized = false;
  public navItems = [];

  constructor(private loginService: LoginService) {
    this.navItems = navItems;
  }

  ngOnInit(): void {
  }

  toggleMinimize(e) {
    this.sidebarMinimized = e;
  }
}
