import { INavData } from '@coreui/angular';

export const navItems: INavData[] = [
  {
    name: 'Inicio',
    url: '/'
  },
  {
    name: 'Empleados',
    url: '/empleado',
    icon: 'fas fa-users-cog'
  },
  {
    name: 'Configuraciones',
    icon: 'fas fa-cog',
    children: [
      {
        name: 'Áreas',
        url: '/area',
        icon: 'fas fa-asterisk'
      },
      {
        name: 'Subareas',
        url: '/subarea',
        icon: 'fas fa-asterisk'
      },
      {
        name: 'Tipos Documentos',
        url: '/tipo-documento',
        icon: 'fas fa-asterisk'
      }
    ]
  }
];
