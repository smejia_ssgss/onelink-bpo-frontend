import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { throwError } from 'rxjs';

import { LoginService } from 'src/app/shared/services/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public loginForm: FormGroup;
  public message: string;

  get controls() { return this.loginForm.controls; }

  constructor(private router: Router, private loginService: LoginService) {
    this.message = '';
    this.createForm();
  }

  ngOnInit(): void {
  }

  private createForm() {
    this.loginForm = new FormGroup({
      user: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required)
    });
  }

  onSubmit() {
    const valuesForm = this.loginForm.getRawValue();
    this.loginService.login(valuesForm.user, valuesForm.password).subscribe(result => {
      if (result.logged) {
        this.router.navigate(['/']);
      }
    }, err => {
      this.message = 'Usuario y/o Contraseña Incorrectos.'

      const error = err.message || err.statusText;
      return throwError(error);
    });
  }
}
