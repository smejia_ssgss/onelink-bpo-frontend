import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './core/guards/auth.guard';
import { LoginComponent } from './core/components/login/login.component';
import { FullLayoutComponent } from './core/components/layout/full-layout/full-layout.component';
import { SimpleLayoutComponent } from './core/components/layout/simple-layout/simple-layout.component';

const routes: Routes = [
  {
    path: 'login',
    component: SimpleLayoutComponent,
    pathMatch: 'full',
    children: [
      {
        path: '',
        component: LoginComponent
      },
    ]
  },
  {
    path: '',
    component: FullLayoutComponent,
    data: {
      title: 'Inicio'
    },
    children: [
      {
        path: '',
        canActivate: [AuthGuard],
        loadChildren: () => import('./views/dashboard/dashboard.module').then(mod => mod.DashboardModule)
      },
      {
        path: 'tipo-documento',
        canActivate: [AuthGuard],
        loadChildren: () => import('./views/tipo-documento/tipo-documento.module').then(mod => mod.TipoDocumentoModule)
      },
      {
        path: 'area',
        canActivate: [AuthGuard],
        loadChildren: () => import('./views/area/area.module').then(mod => mod.AreaModule)
      },
      {
        path: 'subarea',
        canActivate: [AuthGuard],
        loadChildren: () => import('./views/subarea/subarea.module').then(mod => mod.SubareaModule)
      },
      {
        path: 'empleado',
        canActivate: [AuthGuard],
        loadChildren: () => import('./views/empleado/empleado.module').then(mod => mod.EmpleadoModule)
      }
    ]
  },
  {
    path: 'pages',
    component: SimpleLayoutComponent,
    data: {
      title: 'Pages'
    },
    children: [
      {
        path: '',
        loadChildren: () => import('./views/pages/pages.module').then(mod => mod.PagesModule)
      }
    ]
  },
  {
    path: '**',
    redirectTo: 'pages/404'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
