import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';

import { MessageDialogComponent } from 'src/app/shared/components/message-dialog/message-dialog.component';
import { TipoDocumentoModel } from 'src/app/shared/models/tipo-documento/tipo-documento.model';
import { MessageDialogData, MessageType } from 'src/app/shared/models/generics/message-dialog-config';
import { TipoDocumentoService } from 'src/app/shared/services/tipodocumento.service';


@Component({
  selector: 'tipo-documento-add-edit',
  templateUrl: './add-edit.component.html'
})
export class AddEditComponent implements OnInit, OnDestroy {

  public isEditView: boolean;
  public addEditTipoDocumentoModelForm: FormGroup;
  get controls() { return this.addEditTipoDocumentoModelForm.controls; }

  private id: number;
  private model: TipoDocumentoModel;

  constructor(private fb: FormBuilder, private activatedRoute: ActivatedRoute, private route: Router, public dialog: MatDialog,
    private TipoDocumentoService: TipoDocumentoService) {
    this.id = null;
    this.model = new TipoDocumentoModel();
    this.createForm();
  }

  ngOnInit(): void {
    if (this.activatedRoute.snapshot.params.id) {
      this.id = this.activatedRoute.snapshot.params.id;
      this.isEditView = this.id ? true : false;
      this.fillView();
    }
  }

  ngOnDestroy(): void {
  }

  private createForm() {
    this.addEditTipoDocumentoModelForm = this.fb.group({
      codigo: this.fb.control(null, [ Validators.required ]),
      nombre: this.fb.control(null, [ Validators.required ])
    });
  }

  private getModelData(): TipoDocumentoModel {
    const formValues = this.addEditTipoDocumentoModelForm.getRawValue();
    const newModel: TipoDocumentoModel = Object.assign(this.model, formValues);
    newModel.codigo = newModel.codigo.toUpperCase();
    newModel.nombre = newModel.nombre.toUpperCase();

    return newModel;
  }

  private fillView() {
    this.TipoDocumentoService.getById(this.id).subscribe(documentType => {
      this.model = documentType;
      this.addEditTipoDocumentoModelForm.setValue({
        codigo: documentType.codigo,
        nombre: documentType.nombre
      });
    });
  }

  private errorDialog(msg: string) {
    const dialogData: MessageDialogData = {
      content: msg,
      type: MessageType.Error
    }
    const dialogRef = this.dialog.open(MessageDialogComponent, {
      width: '400px',
      data: dialogData,
    });
  }

  // Events
  save() {
    const model = this.getModelData();
    if (this.isEditView) {
      this.TipoDocumentoService.update(model).subscribe(result => {
        this.route.navigate(['/tipo-documento']);
      }, error => {
        this.errorDialog("No fue posible editar el tipo documento");
      });
    } else {
      this.TipoDocumentoService.save(model).subscribe(result => {
        this.route.navigate(['/tipo-documento']);
      }, error => {
        this.errorDialog("No fue posible agregar el tipo documento");
      })
    }
  }
}
