// Modules
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

// Components
import { ListComponent } from './list/list.component';
import { AddEditComponent } from './add-edit/add-edit.component';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Tipos documentos'
    },
    children: [
      {
        path: '',
        component: ListComponent,
        data: {
          title: ''
        }
      },
      {
        path: 'editar/:id',
        component: AddEditComponent,
        data: {
          title: 'Editar'
        }
      },
      {
        path: 'agregar',
        component: AddEditComponent,
        data: {
          title: 'Agregar'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TipoDocumentoRoutingModule { }
