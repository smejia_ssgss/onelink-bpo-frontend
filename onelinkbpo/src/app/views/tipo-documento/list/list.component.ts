import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

import { MessageDialogComponent } from 'src/app/shared/components/message-dialog/message-dialog.component';
import { MessageDialogData, MessageType } from 'src/app/shared/models/generics/message-dialog-config';
import { CustomDatatableDirective } from 'src/app/shared/directives/custom-datatable.directive';
import { TipoDocumentoService } from 'src/app/shared/services/tipodocumento.service';
import { TipoDocumentoModel } from 'src/app/shared/models/tipo-documento/tipo-documento.model';

@Component({
  selector: 'tipo-documento-list',
  templateUrl: './list.component.html'
})
export class ListComponent implements OnInit {
  gridData: TipoDocumentoModel[] = [];
  optionsTable: any = {};

  @ViewChild(CustomDatatableDirective, { static: false }) gridDirective: CustomDatatableDirective;

  constructor(public dialog: MatDialog, private TipoDocumentoService: TipoDocumentoService) {
    this.optionsTable = {
      columnDefs: [
        { orderDataType: "dom-text", targets: [0, 1, 2] },
        { orderable: false, targets: 3 }
      ]
    }
  }

  ngOnInit(): void {
    this.getData();
  }

  private getData() {
    this.TipoDocumentoService.getAll().subscribe(this.updateData.bind(this));
  }

  private updateData(data: any[]) {
    this.gridData = data;
    this.gridDirective.renderTable();
  }

  private errorDialog(msg: string) {
    const dialogData: MessageDialogData = {
      content: msg,
      type: MessageType.Error
    }
    const dialogRef = this.dialog.open(MessageDialogComponent, {
      width: '400px',
      data: dialogData,
    });
  }

  confirmDelete(data: any) {
    const dialogData: MessageDialogData = {
      headerText: "Confirmación",
      content: `¿Desea eliminar el registro ${data.name}?`,
      type: MessageType.Confirm
    }
    const dialogRef = this.dialog.open(MessageDialogComponent, {
      width: '400px',
      data: dialogData,
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.TipoDocumentoService.delete(data.id).subscribe(entity => {
          this.getData();
        }, error => {
          this.errorDialog("No fue posible eliminar el tipo documento");
        });
      }
    });
  }
}
