import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';

import { MessageDialogComponent } from 'src/app/shared/components/message-dialog/message-dialog.component';
import { SubareaModel } from 'src/app/shared/models/subarea/subarea.model';
import { MessageDialogData, MessageType } from 'src/app/shared/models/generics/message-dialog-config';
import { SubareaService } from 'src/app/shared/services/subarea.service';
import { AreaModel } from 'src/app/shared/models/area/area.model';
import { AreaService } from 'src/app/shared/services/area.service';

@Component({
  selector: 'subarea-add-edit',
  templateUrl: './add-edit.component.html'
})
export class AddEditComponent implements OnInit, OnDestroy {
  public areaList: AreaModel[];
  public isEditView: boolean;
  public addEditSubareaForm: FormGroup;
  get controls() { return this.addEditSubareaForm.controls; }

  private id: number;
  private model: SubareaModel;

  constructor(private fb: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private route: Router,
    public dialog: MatDialog,
    private subareaService: SubareaService,
    private areaService: AreaService) {
    this.id = null;
    this.model = new SubareaModel();
    this.createForm();
  }

  ngOnInit(): void {
    this.getInitialData();
    if (this.activatedRoute.snapshot.params.id) {
      this.id = this.activatedRoute.snapshot.params.id;
      this.isEditView = this.id ? true : false;
      this.fillView();
    }
  }

  ngOnDestroy(): void {
  }

  private createForm() {
    this.addEditSubareaForm = this.fb.group({
      nombre: this.fb.control(null, [ Validators.required ]),
      areaId: this.fb.control(null, [ Validators.required ])
    });
  }

  private getModelData(): SubareaModel {
    const formValues = this.addEditSubareaForm.getRawValue();
    const newModel: SubareaModel = Object.assign(this.model, formValues);
    newModel.nombre = newModel.nombre.toUpperCase();
    newModel.areaId = newModel.areaId;

    return newModel;
  }

  private getInitialData() {
    this.areaService.getAll().subscribe(types => {
      this.areaList = types;
    });
  }

  private fillView() {
    this.subareaService.getById(this.id).subscribe(subarea => {
      this.model = subarea;
      this.addEditSubareaForm.setValue({
        nombre: subarea.nombre,
        areaId: subarea.areaId
      });
    });
  }

  private errorDialog(msg: string) {
    const dialogData: MessageDialogData = {
      content: msg,
      type: MessageType.Error
    }
    const dialogRef = this.dialog.open(MessageDialogComponent, {
      width: '400px',
      data: dialogData,
    });
  }

  // Events
  save() {
    const model = this.getModelData();
    if (this.isEditView) {
      this.subareaService.update(model).subscribe(result => {
        this.route.navigate(['/subarea']);
      }, error => {
        this.errorDialog("No fue posible editar la subarea");
      });
    } else {
      this.subareaService.save(model).subscribe(result => {
        this.route.navigate(['/subarea']);
      }, error => {
        this.errorDialog("No fue posible agregar la subarea");
      })
    }
  }
}
