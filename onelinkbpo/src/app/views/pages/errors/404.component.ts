import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'not-found',
  templateUrl: './404.component.html',
  styleUrls: ['./404.component.scss']
})
export class P404Component implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
