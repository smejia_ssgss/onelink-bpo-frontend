import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { P404Component } from './errors/404.component';
import { P500Component } from './errors/500.component';
import { PagesRoutingModule } from './page-routing.module';

@NgModule({
  declarations: [
    P404Component,
    P500Component
  ],
  imports: [
    CommonModule,
    PagesRoutingModule
  ]
})
export class PagesModule { }
