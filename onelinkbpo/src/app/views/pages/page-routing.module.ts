import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

import { P404Component } from './errors/404.component';
import { P500Component } from './errors/500.component';


const routes: Routes = [
    {
        path: '404',
        component: P404Component,
        data: {
            title: 'Page 404'
        }
    },
    // {
    //     path: '500',
    //     component: P500Component,
    //     data: {
    //         title: 'Page 500'
    //     }
    // }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PagesRoutingModule { }
