import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';

import { MessageDialogComponent } from 'src/app/shared/components/message-dialog/message-dialog.component';
import { AreaModel } from 'src/app/shared/models/area/area.model';
import { EmpleadoModel } from 'src/app/shared/models/empleado/empleado.model';
import { MessageDialogData, MessageType } from 'src/app/shared/models/generics/message-dialog-config';
import { SubareaModel } from 'src/app/shared/models/subarea/subarea.model';
import { TipoDocumentoModel } from 'src/app/shared/models/tipo-documento/tipo-documento.model';
import { AreaService } from 'src/app/shared/services/area.service';
import { EmpleadoService } from 'src/app/shared/services/empleado.service';
import { SubareaService } from 'src/app/shared/services/subarea.service';
import { TipoDocumentoService } from 'src/app/shared/services/tipodocumento.service';


@Component({
  selector: 'empleado-add-edit',
  templateUrl: './add-edit.component.html'
})
export class AddEditComponent implements OnInit, OnDestroy {
  public tipoDocumentoList: TipoDocumentoModel[];
  public areaList: AreaModel[];
  public subareaList: SubareaModel[];
  public isEditView: boolean;
  public addEditEmpleadoModelForm: FormGroup;
  get controls() { return this.addEditEmpleadoModelForm.controls; }

  private id: number;
  private model: EmpleadoModel;

  constructor(private fb: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private route: Router,
    public dialog: MatDialog,
    private empleadoService: EmpleadoService,
    private tipoDocumentoService: TipoDocumentoService,
    private areaService: AreaService,
    private subareaService: SubareaService) {
    this.id = null;
    this.model = new EmpleadoModel();
    this.createForm();
  }

  ngOnInit(): void {
    this.getInitialData();
    if (this.activatedRoute.snapshot.params.id) {
      this.id = this.activatedRoute.snapshot.params.id;
      this.isEditView = this.id ? true : false;
      this.fillView();
    }
  }

  ngOnDestroy(): void {
  }

  private createForm() {
    this.addEditEmpleadoModelForm = this.fb.group({
      tipoDocumentoId: this.fb.control(null, [ Validators.required ]),
      numeroDocumento: this.fb.control(null, [ Validators.required ]),
      nombres: this.fb.control(null, [ Validators.required ]),
      apellidos: this.fb.control(null, [ Validators.required ]),
      areaId: this.fb.control(null, [ Validators.required ]),
      subareaId: this.fb.control(null, [ Validators.required ]),
    });
  }

  private getModelData(): EmpleadoModel {
    const formValues = this.addEditEmpleadoModelForm.getRawValue();
    const newModel: EmpleadoModel = Object.assign(this.model, formValues);
    return newModel;
  }

  private getInitialData() {
    this.tipoDocumentoService.getAll().subscribe(types => {
      this.tipoDocumentoList = types;
    });
    this.areaService.getAll().subscribe(types => {
      this.areaList = types;
    });
  }

  public getSubareasByArea(areaId) {
    this.subareaService.getAllByAreaId(areaId).subscribe(types => {
      this.subareaList = types;
    });
  }

  private fillView() {
    this.empleadoService.getById(this.id).subscribe(empleado => {
      this.model = empleado;

      if (this.isEditView == true) {
        this.getSubareasByArea(empleado.subareas.areaId);
      }

      this.addEditEmpleadoModelForm.setValue({
        tipoDocumentoId: empleado.tipoDocumentoId,
        numeroDocumento: empleado.numeroDocumento,
        nombres: empleado.nombres,
        apellidos: empleado.apellidos,
        areaId: empleado.subareas.areaId,
        subareaId: empleado.subareaId
      });
    });
  }

  private errorDialog(msg: string) {
    const dialogData: MessageDialogData = {
      content: msg,
      type: MessageType.Error
    }
    const dialogRef = this.dialog.open(MessageDialogComponent, {
      width: '400px',
      data: dialogData,
    });
  }

  // Events
  save() {
    const model = this.getModelData();
    if (this.isEditView) {
      this.empleadoService.update(model).subscribe(result => {
        this.route.navigate(['/empleado']);
      }, error => {
        this.errorDialog("No fue posible editar el empleado");
      });
    } else {
      this.empleadoService.save(model).subscribe(result => {
        this.route.navigate(['/empleado']);
      }, error => {
        this.errorDialog("No fue posible agregar el empleado");
      })
    }
  }
}
