import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';

import { MessageDialogComponent } from 'src/app/shared/components/message-dialog/message-dialog.component';
import { AreaModel } from 'src/app/shared/models/area/area.model';
import { MessageDialogData, MessageType } from 'src/app/shared/models/generics/message-dialog-config';
import { AreaService } from 'src/app/shared/services/area.service';


@Component({
  selector: 'area-add-edit',
  templateUrl: './add-edit.component.html'
})
export class AddEditComponent implements OnInit, OnDestroy {

  public isEditView: boolean;
  public addEditAreaModelForm: FormGroup;
  get controls() { return this.addEditAreaModelForm.controls; }

  private id: number;
  private model: AreaModel;

  constructor(private fb: FormBuilder, private activatedRoute: ActivatedRoute, private route: Router, public dialog: MatDialog,
    private areaService: AreaService) {
    this.id = null;
    this.model = new AreaModel();
    this.createForm();
  }

  ngOnInit(): void {
    if (this.activatedRoute.snapshot.params.id) {
      this.id = this.activatedRoute.snapshot.params.id;
      this.isEditView = this.id ? true : false;
      this.fillView();
    }
  }

  ngOnDestroy(): void {
  }

  private createForm() {
    this.addEditAreaModelForm = this.fb.group({
      nombre: this.fb.control(null, [ Validators.required ])
    });
  }

  private getModelData(): AreaModel {
    const formValues = this.addEditAreaModelForm.getRawValue();
    const newModel: AreaModel = Object.assign(this.model, formValues);
    newModel.nombre = newModel.nombre.toUpperCase();

    return newModel;
  }

  private fillView() {
    this.areaService.getById(this.id).subscribe(area => {
      this.model = area;
      this.addEditAreaModelForm.setValue({
        nombre: area.nombre
      });
    });
  }

  private errorDialog(msg: string) {
    const dialogData: MessageDialogData = {
      content: msg,
      type: MessageType.Error
    }
    const dialogRef = this.dialog.open(MessageDialogComponent, {
      width: '400px',
      data: dialogData,
    });
  }

  // Events
  save() {
    const model = this.getModelData();
    if (this.isEditView) {
      this.areaService.update(model).subscribe(result => {
        this.route.navigate(['/area']);
      }, error => {
        this.errorDialog("No fue posible editar el área");
      });
    } else {
      this.areaService.save(model).subscribe(result => {
        this.route.navigate(['/area']);
      }, error => {
        this.errorDialog("No fue posible agregar el área");
      })
    }
  }
}
