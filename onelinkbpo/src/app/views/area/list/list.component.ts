import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

import { MessageDialogComponent } from 'src/app/shared/components/message-dialog/message-dialog.component';
import { MessageDialogData, MessageType } from 'src/app/shared/models/generics/message-dialog-config';
import { CustomDatatableDirective } from 'src/app/shared/directives/custom-datatable.directive';
import { AreaService } from 'src/app/shared/services/area.service';
import { AreaModel } from 'src/app/shared/models/area/area.model';

@Component({
  selector: 'area-list',
  templateUrl: './list.component.html'
})
export class ListComponent implements OnInit {
  gridData: AreaModel[] = [];
  optionsTable: any = {};

  @ViewChild(CustomDatatableDirective, { static: false }) gridDirective: CustomDatatableDirective;

  constructor(
    public dialog: MatDialog,
    private areaService: AreaService
  ){
    this.optionsTable = {
      columnDefs: [
        { orderDataType: "dom-text", targets: [0, 1] },
        { orderable: false, targets: 2 }
      ]
    }
  }

  ngOnInit(): void {
    this.getData();
  }

  private getData() {
    this.areaService.getAll().subscribe(this.updateData.bind(this));
  }

  private updateData(data: any[]) {
    this.gridData = data;
    this.gridDirective.renderTable();
  }

  private errorDialog(msg: string) {
    const dialogData: MessageDialogData = {
      content: msg,
      type: MessageType.Error
    }
    const dialogRef = this.dialog.open(MessageDialogComponent, {
      width: '400px',
      data: dialogData,
    });
  }

  confirmDelete(data: any) {
    const dialogData: MessageDialogData = {
      headerText: "Confirmación",
      content: `¿Desea eliminar el registro ${data.name}?`,
      type: MessageType.Confirm
    }
    const dialogRef = this.dialog.open(MessageDialogComponent, {
      width: '400px',
      data: dialogData,
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.areaService.delete(data.id).subscribe(entity => {
          this.getData();
        }, error => {
          this.errorDialog("No fue posible eliminar el área");
        });
      }
    });
  }
}
